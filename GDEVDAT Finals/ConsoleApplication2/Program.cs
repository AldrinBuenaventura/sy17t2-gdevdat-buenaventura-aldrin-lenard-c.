﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public class User
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return string.Format("Username: {0}, Email: {1}",
                Username, Email);
        }
    }
    public class Character
    {
        public string Username { get; set; }
        public string Charactername { get; set; }
        public int Strength { get; set; }
        public int Agility { get; set; }
        public int Intelligence { get; set; }
        public int Level { get; set; }
        public override string ToString()
        {
            return string.Format("Username: {0}, Character Name: {1}, Level: {2}, Strength: {3}, Agility: {4}, Intelligence {5}",
                Username, Charactername, Level, Strength, Agility, Intelligence);
        }
    }

    class Program
    {
        static void TryCreateTable()
        {
            string connectionString = "Data Source=TAFT-CL527;Initial Catalog=BuenaventuraAldrinTG001;User ID=sa;Password=benilde";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                "CREATE TABLE UserTable (ID int IDENTITY(1,1) PRIMARY KEY, Username VARCHAR(255) UNIQUE, Email VARCHAR(255) UNIQUE)", con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine("Table not created.");
                }
            }
        }
        static void TryCreateTableChar()
        {
            string connectionString = "Data Source=TAFT-CL527;Initial Catalog=BuenaventuraAldrinTG001;User ID=sa;Password=benilde";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                "CREATE TABLE CharTable (ID int IDENTITY(1,1) PRIMARY KEY, Username VARCHAR(255) UNIQUE, Charactername VARCHAR(255) UNIQUE, Level int, Strength int, Agility int, Intelligence int)", con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine("Table not created.");
                }
            }
        }
        static void AddUser(string Username, string Email)
        {
            string connectionString = "Data Source=TAFT-CL527;Initial Catalog=BuenaventuraAldrinTG001;User ID=sa;Password=benilde";
            using (SqlConnection con = new SqlConnection(
                connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                        "INSERT INTO UserTable VALUES(@Username, @Email)", con))
                    {
                        command.Parameters.Add(new SqlParameter("Username", Username));
                        command.Parameters.Add(new SqlParameter("Email", Email));
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine("Username or Email already taken");
                }
            }
        }

        static void AddChar(string Username, string Charactername, int Level, int Strength, int Agility, int Intelligence)
        {
            string connectionString = "Data Source=TAFT-CL527;Initial Catalog=BuenaventuraAldrinTG001;User ID=sa;Password=benilde";
            using (SqlConnection con = new SqlConnection(
                connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                        "INSERT INTO CharTable VALUES(@Username, @Charactername, @Level, @Strength, @Agility, @Intelligence)", con))
                    {
                        command.Parameters.Add(new SqlParameter("Username", Username));
                        command.Parameters.Add(new SqlParameter("Charactername", Charactername));
                        command.Parameters.Add(new SqlParameter("Level", Level));
                        command.Parameters.Add(new SqlParameter("Strength", Strength));
                        command.Parameters.Add(new SqlParameter("Agility", Agility));
                        command.Parameters.Add(new SqlParameter("Intelligence", Intelligence));
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine("Username or Character Name already taken");
                }
            }
        }

        static void DisplayUsers(string input)
        {
            List<User> users = new List<User>();
            string connectionString = "Data Source=TAFT-CL527;Initial Catalog=BuenaventuraAldrinTG001;User ID=sa;Password=benilde";
            using (SqlConnection con = new SqlConnection(
                connectionString))
            {
                con.Open();

                using (SqlCommand command = new SqlCommand("SELECT * FROM UserTable WHERE Username = '" + input + "'", con))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string username = reader.GetString(1);  
                        string email = reader.GetString(2); 
                        users.Add(new User() { Username = username, Email = email });
                    }
                }
            }
            foreach (User user in users)
            {
                Console.WriteLine(user);
            }
        }

        static void DisplayChar(string input)
        {
            List<Character> characters = new List<Character>();
            string connectionString = "Data Source=TAFT-CL527;Initial Catalog=BuenaventuraAldrinTG001;User ID=sa;Password=benilde";
            using (SqlConnection con = new SqlConnection(
                connectionString))
            {
                con.Open();

                using (SqlCommand command = new SqlCommand("SELECT * FROM CharTable WHERE Username = '" + input + "'", con))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string username = reader.GetString(1);
                        string charname = reader.GetString(2);
                        int level = reader.GetInt32(3);
                        int strength = reader.GetInt32(4);
                        int agility = reader.GetInt32(5);
                        int intelligence = reader.GetInt32(6);
                        characters.Add(new Character() { Username = username, Charactername = charname, Level = level, Strength = strength, Agility = agility, Intelligence = intelligence });
                    }
                }
            }
            foreach (Character character in characters)
            {
                Console.WriteLine(character);
            }
        }

        static void Main(string[] args)
        {
            Program.TryCreateTable();
            Program.TryCreateTableChar();
            while (true)
            {
                Console.WriteLine("INPUT:\tRegister('0')\tor\tDisplay Email('1')\tor\tCreate Character('2')\tor\tDisplay Character('3')");
                string[] input = Console.ReadLine().Split(',');
                try
                {
                    char c = char.ToLower(input[0][0]);
                    if (c == '0')
                    {
                        Console.WriteLine("INPUT:\tUsername,Email");
                        string[] input2 = Console.ReadLine().Split(',');
                        string Username = input2[0];
                        string Email = input2[1];
                        AddUser(Username, Email);
                    }
                    if (c == '1')
                    {
                        Console.WriteLine("INPUT:\tUsername");
                        string input2 = Console.ReadLine();
                        DisplayUsers(input2);
                        continue;
                    }
                    if (c == '2')
                    {
                        Console.WriteLine("INPUT:\tUsername");
                        string Username = Console.ReadLine();
                        Console.WriteLine("INPUT:\tCharacter Name");
                        string Charname = Console.ReadLine();
                        Console.WriteLine("INPUT:\tLevel");
                        int Level = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("INPUT:\tStrength");
                        int Strength = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("INPUT:\tAgility");
                        int Agility = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("INPUT:\tIntelligence");
                        int Intelligence = Convert.ToInt32(Console.ReadLine());
                        AddChar(Username, Charname, Level, Strength, Agility, Intelligence);
                        continue;
                    }
                    if (c == '3')
                    {
                        Console.WriteLine("INPUT:\tUsername");
                        string input3 = Console.ReadLine();
                        DisplayChar(input3);
                        continue;
                    }
                    
                }
                catch
                {
                    Console.WriteLine("Input error");
                }
            }
        }
    }
    }

